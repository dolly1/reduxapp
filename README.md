# Customers List

Application to manage customers


Application is based on API and API URL is

http://localhost:3001/Customers

Will return the following kind of result:

{
    "name": "dolly",
    "email": "dolly@improwised.com",
    "contact_no": "9228286922",
    "country": "India",
    "pan_no": "awjpe2345p",
    "url": "https://google.com",
    "gender": "female",
    "languages": "english",
    "address": "sdfsdfdsfsdf",
    "id": 4
}

## Result types

All results are returned in JSON.

## Installation

# install depedency

npm install


### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.



