module.exports = {
  moduleFileExtensions: ['js', 'jsx', 'json', 'vue'],

  setupFiles: [
      "dotenv/config"
    ],
  transform: {
   "^.+\\.js$": "babel-jest",
      ".+\\.(css|styl|less|sass|scss)$": "jest-transform-css"
  },
  moduleNameMapper: {
    "\\.(css|less|sass|scss)$": "<rootDir>/__mocks__/styleMock.js",
    "\\.(gif|ttf|eot|svg)$": "<rootDir>/__mocks__/fileMock.js",
    '^@/(.*)$': '<rootDir>/pages/$1'
  },
  testMatch: [
    '<rootDir>/(_tests_/**/*.test.(js|jsx|ts|tsx)|**/__tests__/*.(js|jsx|ts|tsx))'
  ],
  transformIgnorePatterns: ['<rootDir>/node_modules/']
};