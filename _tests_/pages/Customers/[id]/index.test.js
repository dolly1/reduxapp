import React from 'react';
import { shallow,configure,componentDidMount,mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import 'regenerator-runtime/runtime';
import Index from '../../../../pages/Customers/[id]/index.js'
import {  Modal } from 'react-bootstrap'
import findDOMNode from 'react-dom'
import configureStore from 'redux-mock-store'


configure({ adapter: new Adapter() });


const middlewares = []
const mockStore = configureStore(middlewares)

describe('Index', () => {
  var wrapper
  it('View data based on parameter',async (done)=> {
    const query = { id : '4' };
    const props = await Index.getInitialProps({query})
    expect(props).toEqual( {Customer:
         { name: 'dolly',
           email: 'dolly@improwised.com',
           contact_no: '9228286922',
           country: 'India',
           pan_no: 'awjpe2345p',
           url: 'https://google.com',
           gender: 'female',
           languages: 'english',
           address: 'sdfsdfdsfsdf',
           id: 4 },
        isServer: false,
        id: '4'});

    const initialState = props.Customer
    const store = mockStore(initialState)

    expect(store.getState()).toEqual(
      { name: 'dolly',
         email: 'dolly@improwised.com',
         contact_no: '9228286922',
         country: 'India',
         pan_no: 'awjpe2345p',
         url: 'https://google.com',
         gender: 'female',
         languages: 'english',
         address: 'sdfsdfdsfsdf',
      id: 4 });

    done();

  })
})