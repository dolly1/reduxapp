import React from 'react';
import { shallow,configure,componentDidMount,mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import 'regenerator-runtime/runtime';
import Customer from '../../pages/Customers.js'
import {  Modal } from 'react-bootstrap'
import findDOMNode from 'react-dom'
import configureStore from 'redux-mock-store'



configure({ adapter: new Adapter() });


const middlewares = []
const mockStore = configureStore(middlewares)

const req = jest.fn()

const loadData = () => ({ type: 'Load_Data' })


describe('Customer', () => {
  var wrapper
  it('View data based on props',async (done)=> {
    const props = await Customer.getInitialProps({req})
    expect(props.Customers[2]).toEqual(
       { name: 'dolly',
         email: 'dolly@improwised.com',
         contact_no: '9228286922',
         country: 'India',
         pan_no: 'awjpe2345p',
         url: 'https://google.com',
         gender: 'female',
         languages: 'english',
         address: 'sdfsdfdsfsdf',
         id: 4 } );


  // Initialize mockstore with empty state
    const initialState = props.Customers
    const store = mockStore(initialState)

    expect(store.getState()[2]).toEqual(
      { name: 'dolly',
         email: 'dolly@improwised.com',
         contact_no: '9228286922',
         country: 'India',
         pan_no: 'awjpe2345p',
         url: 'https://google.com',
         gender: 'female',
         languages: 'english',
         address: 'sdfsdfdsfsdf',
      id: 4 });

    // store.dispatch(loadData(props.Customers))
    // const actions = store.getActions()
    // const expectedPayload = { type: 'Load_Data' }
    // expect(actions).toEqual([expectedPayload])

    done();
  })

})