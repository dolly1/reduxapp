import React from 'react';
import { shallow,configure } from 'enzyme';
import Home from '../../pages/index.js';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

describe('Pages', () => {
  describe('Index', () => {
    it('should render without throwing an error', function () {
    const wrapper = shallow(<Home />);
    expect(wrapper.exists()).toBe(true)
    expect(wrapper.find('h1').text()).toEqual("Welcome to Next.js!");
    })
  })
})