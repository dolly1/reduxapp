import React from 'react';
import { shallow,configure,componentDidMount,mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import 'regenerator-runtime/runtime';
import App,{url} from '../../components/CreateCustomer';
import {  Modal } from 'react-bootstrap'
import findDOMNode from 'react-dom'

configure({ adapter: new Adapter() });


const axios = require('axios');
const viewurl = url + "4";

jest.mock('axios', () => {
  const Customers = { "name": "dolly",
      "email": "dolly@improwised.com",
      "contact_no": "9228286922",
      "country": "India",
      "pan_no": "awjpe2345p",
      "url": "https://google.com",
      "gender": "female",
      "languages": "english,gujarati",
      "address": "asdasd",
    };
  return {
    patch: jest.fn((viewurl,Customers) => {
        return Promise.resolve({
          data: Customers
        });
    })
  };
});

const customers1={"name": "dolly",
  "email": "dolly@improwised.com",
  "contact_no": "9228286922",
  "country": "India",
  "pan_no": "awjpe2345p",
  "url": "https://google.com",
  "gender": "female",
  "languages": "english,gujarati",
  "address": "asdasd",
  "id": 4};

const app = shallow(<App data={customers1}  pathdata="/Customers/[id]/[Edit]"/>);

it('view customer for edit on page load', ()=> {
  expect(app.find('Form').html()).toEqual('<form class=\"\"><div class=\"form-row\"><div class=\"form-group col\"><label class=\"form-label\" for=\"formGridName\">Name</label><input placeholder=\"Enter customer&#x27;s name\" name=\"name\" id=\"formGridName\" class=\"form-control\" value=\"dolly\"/></div><div class=\"form-group col\"><label class=\"form-label\" for=\"formGridEmail\">Email</label><input type=\"email\" placeholder=\"Enter email\" name=\"email\" id=\"formGridEmail\" class=\"form-control\" value=\"dolly@improwised.com\"/></div></div><div class=\"form-row\"><div class=\"form-group col\"><label class=\"form-label\" for=\"formGridContact\">Contact No</label><input placeholder=\"Enter customer&#x27;s Contact No number\" name=\"contact_no\" id=\"formGridContact\" class=\"form-control\" value=\"9228286922\"/></div><div class=\"form-group col\"><label class=\"form-label\" for=\"formGridcountry\">Country</label><select id=\"formGridcountry\" class=\"form-control\"><option selected=\"\" value=\"India\">India</option><option value=\"US\">US</option><option value=\"Australia\">Australia</option><option value=\"Japan\">Japan</option></select></div></div><div class=\"form-row\"><div class=\"form-group col\"><label class=\"form-label\" for=\"formGridPanno\">Pan No</label><input placeholder=\"Enter customer&#x27;s Pan No\" name=\"pan_no\" id=\"formGridPanno\" class=\"form-control\" value=\"awjpe2345p\"/></div><div class=\"form-group col\"><label class=\"form-label\" for=\"formGridUrl\">Url</label><input placeholder=\"Enter URL\" name=\"url\" id=\"formGridUrl\" class=\"form-control\" value=\"https://google.com\"/></div></div><div class=\"form-row\"><div class=\"form-group col\"><label class=\"form-label\" for=\"formGridaddress\">Address</label><input placeholder=\"Enter customer&#x27;s Address\" name=\"address\" id=\"formGridaddress\" class=\"form-control\" value=\"asdasd\"/></div></div><div class=\"form-row\"><div class=\"form-group col\"><label class=\"form-label\" for=\"formGridGender\">Gender</label><div class=\"mb-3\"><div class=\"custom-control custom-radio custom-control-inline\"><input type=\"radio\" name=\"gender\" value=\"male\" id=\"male\" class=\"custom-control-input\"/><label title=\"\" type=\"checkbox\" for=\"male\" class=\"custom-control-label\">Male</label></div><div class=\"custom-control custom-radio custom-control-inline\"><input type=\"radio\" name=\"gender\" value=\"female\" checked=\"\" id=\"female\" class=\"custom-control-input\"/><label title=\"\" type=\"checkbox\" for=\"female\" class=\"custom-control-label\">Female</label></div></div></div><div class=\"form-group col\"><label class=\"form-label\" for=\"formGridUrl\">Languages</label><div class=\"mb-3\"><div class=\"custom-control custom-checkbox custom-control-inline\"><input type=\"checkbox\" checked=\"\" id=\"english\" class=\"custom-control-input\"/><label title=\"\" type=\"checkbox\" for=\"english\" class=\"custom-control-label\">English</label></div><div class=\"custom-control custom-checkbox custom-control-inline\"><input type=\"checkbox\" id=\"hindi\" class=\"custom-control-input\"/><label title=\"\" type=\"checkbox\" for=\"hindi\" class=\"custom-control-label\">Hindi</label></div><div class=\"custom-control custom-checkbox custom-control-inline\"><input type=\"checkbox\" checked=\"\" id=\"gujarati\" class=\"custom-control-input\"/><label title=\"\" type=\"checkbox\" for=\"gujarati\" class=\"custom-control-label\">Gujarati</label></div></div></div></div><div class=\"d-flex justify-content-center form-row\"><button type=\"button\" class=\"btn btn-primary\">Submit</button> <button type=\"reset\" class=\"btn btn-primary\">Reset</button></div></form>');
});

it('update data on submit event', async (done) => {

  const data = { "name": "dolly",
    "email": "dolly@improwised.com",
    "contact_no": "9228286922",
    "country": "India",
    "pan_no": "awjpe2345p",
    "url": "https://google.com",
    "gender": "female",
    "languages": "english,gujarati",
    "address": "asdasd"
  };

  app.find('Button').at(0).simulate('click');
  expect(axios.patch).toHaveBeenCalled();

  expect(axios.patch).toHaveBeenCalledWith(viewurl,data);
  done();

});

it('Reset data on Reset event',() => {
   const button = app.find('Button').at(1).simulate('click');
   expect(app.state().name).toEqual("");
});

