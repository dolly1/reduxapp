import React from 'react';
import { shallow,configure,componentDidMount,mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import 'regenerator-runtime/runtime';
import App,{url} from '../../components/Customers';
import ConfirmModel from '../../components/ConfirmModel'
import {  Modal } from 'react-bootstrap'
import findDOMNode from 'react-dom'

configure({ adapter: new Adapter() });


  const customers1=[{"name": "dolly",
      "email": "dolly@improwised.com",
      "contact_no": "9228286922",
      "country": "India",
      "pan_no": "awjpe2345p",
      "url": "https://google.com",
      "gender": "female",
      "languages": "english,gujarati",
      "address": "asdasd",
      "id": 4}];


const app = shallow(<App  Customers={customers1}/>);
it('View data based on props',async (done)=> {

       //check first child row html
      expect(app.find('.text-center tbody tr:first-child').html()).toEqual('<tr><td><a href="/Customers/4">dolly</a></td><td>dolly@improwised.com</td><td>9228286922</td><td>female</td><td><button type="button" class="ml-2 btn btn-info"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="eye" class="svg-inline--fa fa-eye fa-w-18 " role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M572.52 241.4C518.29 135.59 410.93 64 288 64S57.68 135.64 3.48 241.41a32.35 32.35 0 0 0 0 29.19C57.71 376.41 165.07 448 288 448s230.32-71.64 284.52-177.41a32.35 32.35 0 0 0 0-29.19zM288 400a144 144 0 1 1 144-144 143.93 143.93 0 0 1-144 144zm0-240a95.31 95.31 0 0 0-25.31 3.79 47.85 47.85 0 0 1-66.9 66.9A95.78 95.78 0 1 0 288 160z"></path></svg></button><button type="button" class="ml-2 btn btn-secondary"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="edit" class="svg-inline--fa fa-edit fa-w-18 " role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M402.6 83.2l90.2 90.2c3.8 3.8 3.8 10 0 13.8L274.4 405.6l-92.8 10.3c-12.4 1.4-22.9-9.1-21.5-21.5l10.3-92.8L388.8 83.2c3.8-3.8 10-3.8 13.8 0zm162-22.9l-48.8-48.8c-15.2-15.2-39.9-15.2-55.2 0l-35.4 35.4c-3.8 3.8-3.8 10 0 13.8l90.2 90.2c3.8 3.8 10 3.8 13.8 0l35.4-35.4c15.2-15.3 15.2-40 0-55.2zM384 346.2V448H64V128h229.8c3.2 0 6.2-1.3 8.5-3.5l40-40c7.6-7.6 2.2-20.5-8.5-20.5H48C21.5 64 0 85.5 0 112v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V306.2c0-10.7-12.9-16-20.5-8.5l-40 40c-2.2 2.3-3.5 5.3-3.5 8.5z"></path></svg>  </button><button type="button" class="ml-2 btn btn-danger"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="trash" class="svg-inline--fa fa-trash fa-w-14 " role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M432 32H312l-9.4-18.7A24 24 0 0 0 281.1 0H166.8a23.72 23.72 0 0 0-21.4 13.3L136 32H16A16 16 0 0 0 0 48v32a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16V48a16 16 0 0 0-16-16zM53.2 467a48 48 0 0 0 47.9 45h245.8a48 48 0 0 0 47.9-45L416 128H32z"></path></svg></button></td></tr>');

      //view link test case
      const VLink = app.find('Link').at(2);
      expect(VLink.props()["as"]).toEqual("/Customers/4")

      //Edit link test case
      const ELink = app.find('Link').at(3);
      expect(ELink.props()["as"]).toEqual("/Customers/4/edit")

      //delete link test case

      app.find('Button').at(3).simulate('click');
      expect(app.state().isOpen).toBe(true);

      const wrapper = shallow(<ConfirmModel />);
      expect(wrapper.childAt(0).html()).toEqual('<div class="modal-header"><div class="modal-title h4">Are you sure ?</div><button type="button" class="close"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button></div>');
      expect(wrapper.childAt(1).html()).toEqual('<div class="modal-body">Would you like to delete this customer?</div>');
      expect(wrapper.childAt(2).html()).toEqual('<div class="modal-footer"><button type="button" class="btn btn-secondary">Cancel</button><button type="button" class="btn btn-primary"> Ok </button></div>');
      done();

});

it('Check add event link',()=> {
  const Link = app.find('Link').at(0);
  expect(Link.length).toBe(1);
  expect(Link.props()["href"]).toEqual("/Customers/New")
});

