import  * as Actions from '../../action'

describe('actions', () => {
  it('should create an action to fetch. data', () => {

    const Customers=[{"name": "dolly",
      "email": "dolly@improwised.com",
      "contact_no": "9228286922",
      "country": "India",
      "pan_no": "awjpe2345p",
      "url": "https://google.com",
      "gender": "female",
      "languages": "english,gujarati",
      "address": "asdasd",
      "id": 4}];


    const expectedAction = {
      type: "Load_Data",
      Customers:Customers
    }
    expect(Actions.loadData(Customers)).toEqual(expectedAction)
  })

  it('should create an action to fetch particular customer data', () => {
    const Customer=[{"name": "dolly",
      "email": "dolly@improwised.com",
      "contact_no": "9228286922",
      "country": "India",
      "pan_no": "awjpe2345p",
      "url": "https://google.com",
      "gender": "female",
      "languages": "english,gujarati",
      "address": "asdasd",
      "id": 4}];

    const expectedAction = {
      type: "Load_Data_Id",
      Customer:Customer
    }
    expect(Actions.loadDataById(Customer)).toEqual(expectedAction)
  })

})