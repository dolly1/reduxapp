let axios = require('axios');


// reducer to indicate we have received all our data from the api
export let loadData = (Customers) => {
    return {
        type : 'Load_Data',
        Customers : Customers
    }
}


export let loadDataById = (Customer) => {

    return {
        type : 'Load_Data_Id',
        Customer : Customer
    }
}

//here we actually do the fetching
export function fetchData() {
    let url = process.env.API_URL+'Customers/'
    return (dispatch) => {
        return axios.get(url).then(
            (response) => {
                let Customers = response.data
                dispatch(loadData(Customers))
            },
            (err) => {
                console.log(err);
            }
        )
    }
}


export function fetchDataById(id) {
    let url = process.env.API_URL+'Customers/' + id
    return (dispatch) => {
        return axios.get(url).then(
            (response) => {
                let Customer = response.data
                dispatch(loadDataById(Customer))
            },
            (err) => {
                console.log(err);
            }
        )
    }
}