const rootReducer = (state={Customers : [],Customer : {}},action) => {
  switch(action.type){
    case 'Load_Data':
      return Object.assign({}, state, {
        Customers : action.Customers
      })
      break;
    case 'Load_Data_Id':
      return Object.assign({}, state, {
        Customer : action.Customer
      })
      break;
      default:
        return state;
    }
}
export default rootReducer;