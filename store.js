import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import rootReducer from './reducers'

const exampleInitialState = {
  Customers: [],
  Customer: {}
}

export function configureStore (initialState=exampleInitialState) {
  return createStore(
    rootReducer,
    initialState,
    applyMiddleware(thunkMiddleware)
  )
}



export default configureStore;


