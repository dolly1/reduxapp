const withCSS = require("@zeit/next-css");
require('dotenv').config()

module.exports = withCSS({
  env: {
    SKIP_PREFLIGHT_CHECK:process.env.SKIP_PREFLIGHT_CHECK,
    API_URL:process.env.API_URL
  },
})
