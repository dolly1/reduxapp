import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import ViewCustomer from '../ViewCustomer'
import  * as Actions from '../../action/index'

function mapStateToProps(state) {
  return {
    ...state,
  };
}


const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(Actions, dispatch),
  };
};


export default connect(mapStateToProps,mapDispatchToProps)(ViewCustomer);