import React from 'react'
import axios from 'axios'
import { Button, Table, Container, Row, Col } from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEdit, faEye, faTrash } from '@fortawesome/free-solid-svg-icons'
import ConfirmModel from './ConfirmModel'
import PropTypes from 'prop-types'
import Link from 'next/link'
import Router from 'next/router'

export const url =process.env.API_URL+"Customers/";
class Customer extends React.Component {
  constructor (props) {

    super(props)

    this.state = { customers: this.props.Customers,
      isOpen: false,
      id: 0
    }


    this.handleClose = this.handleClose.bind(this)
    this.handleShow = this.handleShow.bind(this)
  }

  handleClose () {
    this.setState({
      isOpen: false
    })
  }


  // componentDidMount () {
  //    this.props.actions.fetchData();
  //    this.setState({ customers: this.props.data })

  //    console.log(customers);
  // }

  handleShow (cid) {
    this.setState({
      isOpen: true,
      id: cid
    })
  }

  async deleteCustomer (id, customers) {

    try
    {
      this.handleClose();
      axios.delete(url + id);
      const index = customers.findIndex(customer => customer.id === id)
      customers.splice(index, 1)
      Router.prefetc('/Customers');
    }
    catch(err)
    {

    };
  }

  render () {
    return (
        <>
       <Container className ="pt-5">
         <Row className ="py-3">
           <Col><h2>Customers</h2></Col>
           <Col >
            <>
              <Link href="/Customers/New"  as="/Customers/New">
              <Button variant="outline-success" className="float-right" >Add New</Button></Link></ >
           </Col>
         </Row>
         {this.state.customers.length === 0 && (
           <Container className="text-center">
             <h2>No customer found at the moment</h2>
           </Container>
         )}
         {this.state.customers.length !== 0 && (
           <Table striped bordered hover className="text-center">
             <thead>
               <tr>
                 <th>Name</th>
                 <th>Email</th>
                 <th>Contact No</th>
                 <th>Gender</th>
                 <th>Actions</th>
               </tr>
             </thead>
             <tbody>
              {
                this.state.customers && this.state.customers.map(customer =>
                <tr key={customer.id}>
                  <td><><Link href="/Customers/[id]"  as={`/Customers/${customer.id}`}><a>{customer.name}</a></Link></></td>
                  <td>{customer.email}</td>
                  <td>{customer.contact_no}</td>
                  <td>{customer.gender}</td>
                  <td>
                    <>
                    <Link href="/Customers/[id]"  as={`/Customers/${customer.id}`}>
                    <Button variant="info" className ="ml-2" ><FontAwesomeIcon icon={faEye} /></Button></Link></>

                    <>
                    <Link href="/Customers/[id]/[Edit]"  as={`/Customers/${customer.id}/edit`}>
                    <Button variant="secondary" className ="ml-2"><FontAwesomeIcon icon={faEdit} />  </Button></Link></>

                    <Button variant="danger" onClick={() => this.handleShow(customer.id)} className ="ml-2" ><FontAwesomeIcon icon={faTrash} /></Button>
                   </td>
                 </tr>
               )}
             </tbody>
           </Table>
         )}
         <ConfirmModel isOpen={this.state.isOpen} handleClose={this.handleClose} deleteCustomer={this.deleteCustomer} id={this.state.id}
            customers={this.state.customers}/>
       </Container>
     </>
    )
  }
}

Customer.propTypes = {
  history : PropTypes.any
}
export default Customer
