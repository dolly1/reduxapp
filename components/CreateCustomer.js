import React,{ Component } from 'react';
import axios from 'axios';
import SimpleReactValidator from 'simple-react-validator';
import { Button, Container ,Col,Form,Alert } from 'react-bootstrap';
import PropTypes from 'prop-types'
import Router from 'next/router'

export const url = process.env.API_URL +"Customers/";
class CreateCustomer extends Component {

    constructor(props) {
    super(props);
    let hindi = false;
    let english= false;
    let gujarati = false;
    if(this.props.pathdata == "/Customers/[id]/[Edit]")
    {
      for (let lan in this.props.data.languages.split(',')) {
        if(props.data.languages.split(',')[lan] === "english")
        {
          english = true;
        }
        else if(props.data.languages.split(',')[lan] === "hindi")
        {
           hindi = true;
        }
        else if(props.data.languages.split(',')[lan] === "gujarati")
        {
           gujarati = true;
        }
      }
      this.state = {
        id: props.data.id,
        name:props.data.name,
        email: props.data.email,
        contact_no:props.data.contact_no,
        gender:props.data.gender,
        country:props.data.country,
        address:props.data.address,
        pan_no:props.data.pan_no,
        url:props.data.url,
        languages:props.data.languages,
        pagename:'Edit Customer',
        buttonmsg:'Update Customer',
        message:"Customer's details has been updated successfully",
        ajaxError: 'There was a server error the prevented the form from submitting.',
        values: [],
        loading: false,
        submitSuccess: false,
        english: english,
        hindi: hindi,
        gujarati: gujarati,
      }

    }
    else
    {
      this.state = {
        id: '',
        name: '',
        email: '',
        contact_no: '',
        gender:'male',
        country:'India',
        address: '',
        pan_no:'',
        url:'',
        languages:'',
        values: [],
        loading: false,
        submitSuccess: false,
        english: false,
        hindi: false,
        gujarati: false,
        pagename:'Create Customer',
        buttonmsg:'Create Customer',
        message:"Customer's details has been added successfully",
        ajaxError: 'There was a server error the prevented the form from submitting.'
      }
    }
    this.validator = new SimpleReactValidator({
      autoForceUpdate: this,
      className: 'text-danger',
      messages: {
        email: 'Enter Valid Email.',
        phone: 'Enter Valid Contact No',
        required: 'The :attribute is required'
      },
      validators: {
        pan_no: {
          message: 'Enter Valid Pan No.',
          rule: function(val, params, validator) {
            // eslint-disable-next-line
            return validator.helpers.testRegex(val,/^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/) && params.indexOf(val) === -1
          }
        },
         url: {
          message: 'Enter Valid URL.',
          rule: function(val, params, validator) {
            // eslint-disable-next-line
            return validator.helpers.testRegex(val,/^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/) && params.indexOf(val) === -1
          }
        }
      }
    });

    this.processFormSubmission = this.processFormSubmission.bind(this);
    this.handleInputChanges = this.handleInputChanges.bind(this);
    this.toggleChangeEn = this.toggleChangeEn.bind(this);
    this.toggleChangeHn = this.toggleChangeHn.bind(this);
    this.toggleChangeGu = this.toggleChangeGu.bind(this);

  }

  Reset() {
    this.setState({
      name: '',
      email: '',
      contact_no: '',
      country:'india',
      pan_no:'',
      url:'',
      gender:'male',
      languages:'',
      address: ''})
  }

  processFormSubmission() {
    let arr = [];
    for (var key in this.state) {
      if(this.state[key] === true) {
        arr.push(key);
      }
    }
    let data =  arr.toString()
    if (this.validator.allValid()) {
      this.setState({ loading: true });
      const formData = {
        name: this.state.name,
        email: this.state.email,
        contact_no: this.state.contact_no,
        country:this.state.country,
        pan_no:this.state.pan_no,
        url:this.state.url,
        gender:this.state.gender,
        languages:data,
        address: this.state.address
      }
      this.setState({ submitSuccess: true, values: [...this.state.values, formData], loading: false });
      if(this.props.pathdata != "/Customers/[id]/[Edit]")
      {
        try
        {
          axios.post(url, formData);
          setTimeout(() => {
             Router.push('/Customers');
          },1500);
        }
        catch(err)
        {
          console.log(err);
        };
      }
      else
      {
          axios.patch(url + this.state.id, formData);
          setTimeout(() => {
            Router.push('/Customers');
          },1500)
      }
      } else {
      this.validator.showMessages();
    }
  }

  handleInputChanges(e) {
    e.preventDefault();
    this.setState({
      [e.currentTarget.name]: e.currentTarget.value,
    })
  }

  toggleChangeEn ()  {
    this.setState(prevState => ({
      english: !prevState.english,
    }));
  }

  toggleChangeHn ()  {
    this.setState(prevState => ({
      hindi: !prevState.hindi,
    }));
  }

  toggleChangeGu () {
    this.setState(prevState => ({
      gujarati: !prevState.gujarati,
    }));
  }

  render()
  {

    const { submitSuccess, loading } = this.state;
    return(

      <Container className ="pt-5">
        <h2> {this.state.pagename} </h2><br/>
        {submitSuccess && (
          <Alert variant="success">
            {this.state.message} </Alert>
        )}

        <Form>
          <Form.Row>
            <Form.Group as={Col} controlId="formGridName">
              <Form.Label>Name</Form.Label>
              <Form.Control placeholder="Enter customer's name" defaultValue={this.state.name}
              name="name" onChange={(e) => this.handleInputChanges(e)}
              onBlur={() => this.validator.showMessageFor('name')}/>
              {this.validator.message('name', this.state.name, 'required')}
            </Form.Group>

            <Form.Group as={Col} controlId="formGridEmail">
              <Form.Label>Email</Form.Label>
              <Form.Control type="email" placeholder="Enter email" defaultValue={this.state.email}
              name="email" onChange={(e) => this.handleInputChanges(e)}
              onBlur={this.validator.showMessageFor.bind(null, 'email')}/>
              {this.validator.message('email', this.state.email, 'required|email')}
            </Form.Group>
          </Form.Row>

          <Form.Row>
            <Form.Group as={Col} controlId="formGridContact">
              <Form.Label>Contact No</Form.Label>
              <Form.Control placeholder="Enter customer's Contact No number" defaultValue={this.state.contact_no}
              name="contact_no" onChange={(e) => this.handleInputChanges(e)}
              onBlur={() => this.validator.showMessageFor('contact_no')}/>
              {this.validator.message('contact_no', this.state.contact_no, 'required|phone')}
            </Form.Group>
             <Form.Group as={Col} controlId="formGridcountry">
              <Form.Label>Country</Form.Label>
              <Form.Control as="select" defaultValue={this.state.country}
              onChange={(e) => this.handleInputChanges(e)}
              onBlur={() => this.validator.showMessageFor('address')}>
                <option value="India">India</option>
                <option value="US">US</option>
                <option value="Australia">Australia</option>
                <option value="Japan">Japan</option>
              </Form.Control>
            </Form.Group>
          </Form.Row>

          <Form.Row>
            <Form.Group as={Col} controlId="formGridPanno">
              <Form.Label>Pan No</Form.Label>
              <Form.Control placeholder="Enter customer's Pan No" defaultValue={this.state.pan_no}
              name="pan_no" onChange={(e) => this.handleInputChanges(e)}
              onBlur={() => this.validator.showMessageFor('pan_no')}/>
             {this.validator.message('pan_no', this.state.pan_no, 'required|pan_no')}
            </Form.Group>
            <Form.Group as={Col} controlId="formGridUrl">
              <Form.Label>Url</Form.Label>
              <Form.Control  placeholder="Enter URL" defaultValue={this.state.url}
              name="url" onChange={(e) => this.handleInputChanges(e)}
              onBlur={() => this.validator.showMessageFor('url')}/>
              {this.validator.message('url', this.state.url, 'required|url')}
            </Form.Group>
          </Form.Row>

          <Form.Row>
            <Form.Group as={Col} controlId="formGridaddress">
              <Form.Label>Address</Form.Label>
              <Form.Control placeholder="Enter customer's Address" defaultValue={this.state.address}
              name="address" onChange={(e) => this.handleInputChanges(e)}
              onBlur={() => this.validator.showMessageFor('address')}/>
              {this.validator.message('address', this.state.address, 'required')}
            </Form.Group>
          </Form.Row>
           <Form.Row>
            <Form.Group as={Col} controlId="formGridGender">
              <Form.Label>Gender</Form.Label>
              <div key={`custom-inline-radio`} className="mb-3">
                <Form.Check custom inline label="Male" type="radio" name="gender" id="male" value="male" onChange={(e) => this.handleInputChanges(e)} checked={this.state.gender === "male"}/>
                <Form.Check custom inline label="Female" type="radio" name="gender" id="female" value="female" onChange={(e) => this.handleInputChanges(e)} checked={this.state.gender === "female"} />
              </div>
            </Form.Group>
            <Form.Group as={Col} controlId="formGridUrl">
            <Form.Label>Languages</Form.Label>
            <div key={`custom-inline-radio`} className="mb-3">
              <Form.Check custom inline label="English" type="checkbox" id="english" checked={this.state.english}  onChange={this.toggleChangeEn} />
              <Form.Check custom inline label="Hindi" type="checkbox" id="hindi" checked={this.state.hindi}  onChange={this.toggleChangeHn} />
              <Form.Check custom inline label="Gujarati" type="checkbox" id="gujarati" checked={this.state.gujarati}  onChange={this.toggleChangeGu} />
            </div>
            </Form.Group>
          </Form.Row>
           {this.validator.messageWhenPresent(this.state.ajaxError, {element: message => <Alert variant="danger"> {message} </Alert>})}
          <Form.Row  className="d-flex justify-content-center"><Button onClick={this.processFormSubmission.bind(this)} >Submit</Button>&nbsp;
          <Button onClick={this.Reset.bind(this)} type="reset" >Reset</Button>
          </Form.Row>
           {loading &&
             <span className="fa fa-circle-o-notch fa-spin" />
           }
      </Form>
      </Container>

    );
  }
}

CreateCustomer.propTypes = {
  match : PropTypes.any,
  history : PropTypes.any
}


export default CreateCustomer
