webpackHotUpdate("static/development/pages/Customers.js",{

/***/ "./action/index.js":
/*!*************************!*\
  !*** ./action/index.js ***!
  \*************************/
/*! exports provided: loadData, loadDataById, fetchData, fetchDataById */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loadData", function() { return loadData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loadDataById", function() { return loadDataById; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchData", function() { return fetchData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchDataById", function() { return fetchDataById; });
var axios = __webpack_require__(/*! axios */ "./node_modules/axios/index.js"); // reducer to indicate we have received all our data from the api


var loadData = function loadData(Customers) {
  return {
    type: 'Load_Data',
    Customers: Customers
  };
};
var loadDataById = function loadDataById(Customer) {
  return {
    type: 'Load_Data_Id',
    Customer: Customer
  };
}; //here we actually do the fetching

function fetchData() {
  var url = "http://localhost:3001/Customers";
  return function (dispatch) {
    return axios.get(url).then(function (response) {
      var Customers = response.data;
      dispatch(loadData(Customers));
    }, function (err) {
      console.log(err);
    });
  };
}
function fetchDataById(id) {
  var url = "http://localhost:3001/Customers/" + id;
  return function (dispatch) {
    return axios.get(url).then(function (response) {
      var Customer = response.data;
      dispatch(loadDataById(Customer));
    }, function (err) {
      console.log(err);
    });
  };
}

/***/ })

})
//# sourceMappingURL=Customers.js.2d4f4e089e6b64fba569.hot-update.js.map