webpackHotUpdate("static/development/pages/_app.js",{

/***/ "./store.js":
/*!******************!*\
  !*** ./store.js ***!
  \******************/
/*! exports provided: actionTypes, reducer, loadData, loadDataById, fetchData, fetchDataById, initializeStore */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "actionTypes", function() { return actionTypes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "reducer", function() { return reducer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loadData", function() { return loadData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loadDataById", function() { return loadDataById; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchData", function() { return fetchData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchDataById", function() { return fetchDataById; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "initializeStore", function() { return initializeStore; });
/* harmony import */ var _babel_runtime_corejs2_core_js_object_assign__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/assign */ "./node_modules/@babel/runtime-corejs2/core-js/object/assign.js");
/* harmony import */ var _babel_runtime_corejs2_core_js_object_assign__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_core_js_object_assign__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! redux */ "./node_modules/redux/es/redux.js");
/* harmony import */ var redux_devtools_extension__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! redux-devtools-extension */ "./node_modules/redux-devtools-extension/index.js");
/* harmony import */ var redux_devtools_extension__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(redux_devtools_extension__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var redux_thunk__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! redux-thunk */ "./node_modules/redux-thunk/es/index.js");

// import { createStore, applyMiddleware } from 'redux';
// import thunkMiddleware from 'redux-thunk';
// import rootReducer from './reducers'
// const exampleInitialState = {
//   Customers: [],
//   Customer: {}
// }
// export function configureStore (initialState=exampleInitialState) {
//    console.log(initialState);
//   return createStore(
//     rootReducer,
//     initialState,
//     applyMiddleware(thunkMiddleware)
//   )
// }
// export default configureStore;



var exampleInitialState = {
  Customers: [],
  Customer: {}
};
var actionTypes = {
  Load_Data: 'Load_Data',
  Load_Data_Id: 'Load_Data_Id' // REDUCERS

};
var reducer = function reducer() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : exampleInitialState;
  var action = arguments.length > 1 ? arguments[1] : undefined;
  console.log(action.Customers);

  switch (action.type) {
    case actionTypes.Load_Data:
      return _babel_runtime_corejs2_core_js_object_assign__WEBPACK_IMPORTED_MODULE_0___default()({}, state, {
        Customers: action.Customers
      });

    case actionTypes.Load_Data_Id:
      return _babel_runtime_corejs2_core_js_object_assign__WEBPACK_IMPORTED_MODULE_0___default()({}, state, {
        Customer: action.Customer
      });

    default:
      return state;
  }
}; // ACTIONS

var axios = __webpack_require__(/*! axios */ "./node_modules/axios/index.js"); // reducer to indicate we have received all our data from the api


var loadData = function loadData(Customers) {
  return {
    type: actionTypes.Load_Data,
    Customers: Customers
  };
};
var loadDataById = function loadDataById(Customer) {
  return {
    type: actionTypes.Load_Data_Id,
    Customer: Customer
  };
}; //here we actually do the fetching

function fetchData() {
  var url = "http://localhost:3001/Customers";
  return function (dispatch) {
    return axios.get(url).then(function (response) {
      var Customers = response.data;
      dispatch(loadData(Customers));
    }, function (err) {
      console.log(err);
    });
  };
}
function fetchDataById(id) {
  var url = "http://localhost:3001/Customers/" + id;
  return function (dispatch) {
    return axios.get(url).then(function (response) {
      var Customer = response.data;
      dispatch(loadDataById(Customer));
    }, function (err) {
      console.log(err);
    });
  };
}
function initializeStore() {
  var initialState = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : exampleInitialState;
  return Object(redux__WEBPACK_IMPORTED_MODULE_1__["createStore"])(reducer, initialState, Object(redux_devtools_extension__WEBPACK_IMPORTED_MODULE_2__["composeWithDevTools"])(Object(redux__WEBPACK_IMPORTED_MODULE_1__["applyMiddleware"])(redux_thunk__WEBPACK_IMPORTED_MODULE_3__["default"])));
}

/***/ })

})
//# sourceMappingURL=_app.js.4f13cd491b8b2f6256cd.hot-update.js.map