import React from 'react'
import Link from 'next/link'
import Head from 'next/head'
import Header from '../components/Header'
import { connect } from 'react-redux'

const page= (props) => {
  return (
  <div>
    <Header  title="Home"/>
    <div className='example'>
      <h1 className='title'>Welcome to Next.js!</h1>
    </div>
  </div>
)}

export default page;
