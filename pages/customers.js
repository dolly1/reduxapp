import React from 'react'
import Header from '../components/Header'
import { fetchData } from '../action/index'
import Customers from '../components/connectors/Customers'
import { connect } from 'react-redux'




class Customer extends React.Component {
   static async getInitialProps ({ reduxStore, req }) {
    const isServer = !!req;
    await reduxStore.dispatch(fetchData());
    return {}
  }

  render() {
    return (
      <div>
        <Header title="Customers"/>
        <Customers />
      </div>
    );
  }
}



const mapDispatchToProps = { fetchData }
export default connect(
  null,
  mapDispatchToProps
)(Customer)
