import { useRouter } from 'next/router'
import axios from 'axios'
import React from 'react';
import Header from '../../components/Header'
import CreateCustomer from '../../components/CreateCustomer'

const New  = props => {
  return (
    <>
      <Header  title="CreateCustomer"/>
      <CreateCustomer pathdata="/Customers/New"/>
    </>
  )
}

export default New