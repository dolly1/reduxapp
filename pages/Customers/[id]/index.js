import { useRouter } from 'next/router'
import Header from '../../../components/Header'
import ViewCustomer from '../../../components/connectors/ViewCustomer'
import { fetchDataById } from '../../../action/index'
import { connect } from 'react-redux'


class Index extends React.Component {

    static async getInitialProps ({ query,reduxStore, req }) {
    const isServer = !!req;
    await reduxStore.dispatch(fetchDataById(query.id));
    return {}
  }
  render() {
    return (
      <div>
        <Header title="Customers"/>
        <ViewCustomer />
      </div>
    );
  }
}

const mapDispatchToProps = { fetchDataById }
export default connect(
  null,
  mapDispatchToProps
)(Index)
