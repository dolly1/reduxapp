import { useRouter } from 'next/router'
import axios from 'axios'
import Header from '../../../components/Header'
import CreateCustomer from '../../../components/CreateCustomer'

const Edit  = props => {
  return (
    <>
      <Header  title="EditCustomer"/>
        <CreateCustomer data = {props.data} pathdata="/Customers/[id]/[Edit]"/>
    </>
  )
}

Edit.getInitialProps = async function({ query }) {
  const res = await axios.get(process.env.API_URL+'Customers/'+query.id)
  const data = await res.data
  return {
    data: data,
  }
}

export default Edit